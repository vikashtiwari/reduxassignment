import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { fetchUsers } from "../redux/user/userAction";
import "../index.css";

function UserContainer({ userData, fetchUsers }) {
  useEffect(() => {
    fetchUsers();
  }, []);

  const [filtered, setFitered] = useState([]);
  const [id, setId] = useState(null);

  const search = () => {
    setFitered(userData.users.filter((e) => e.id == id));
  };
  return (
    <div className="main">
      <div className="div1">
        {userData.loading ? (
          <h2>Loading</h2>
        ) : userData.error ? (
          <h2>{userData.error}</h2>
        ) : (
          <div>
            <h2>User List</h2>
            <div>
              {userData &&
                userData.users &&
                userData.users.map((user) => <p key={user.id}>{user.name}</p>)}
            </div>
          </div>
        )}
      </div>
      <div className="div2">
        <input type="number" onChange={(e) => setId(e.target.value)} />
        <button onClick={search}>Search</button>
        <br />
        <br />
        <div id="searchedItem">
          {filtered.map((e) => {
            return (
              <div>
                <b>{e.name}</b>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    userData: state,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    fetchUsers: () => dispatch(fetchUsers()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserContainer);
